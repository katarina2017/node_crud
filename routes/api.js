var express = require('express');
var router = express.Router();
var DB = require('../models/db');
router.get('/names',function(req,res){
  DB.find({}, function(err, names) {
    if (err)
      res.send(err);
    res.json(names);
  });
})
router.post('/names',function(req,res){
    var new_document = new DB(req.body);
    new_document.save(function(err,name){
        if (err) res.send(err);
        res.json(name);
    })
})
router.get('/names/:nameId',function(req,res){
  DB.findById(req.params.nameId, function(err, name) {
    if (err)
      res.send(err);
    res.json(name);
  });
})
router.put('/names/:nameId',function(req,res){
  DB.findOneAndUpdate({_id: req.params.nameId}, req.body, {new: true}, function(err, name) {
    if (err)
      res.send(err);
    res.json(name);
  });
})
router.delete('/names/:nameId',function(req,res){
  DB.remove({
    _id: req.params.nameId
  }, function(err, name) {
    if (err)
      res.send(err);
    res.json({ message: 'Name successfully deleted' });
  });
})

module.exports = router;