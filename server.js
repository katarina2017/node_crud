var express = require('express');
var app = express();
var cookieParser = require('cookie-parser')
var bodyParser = require('body-parser');
var path = require('path');
var mongoose = require('mongoose');
var api = require('./routes/api.js');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));

mongoose.connect('mongodb://localhost:27017/test');
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
    console.log('Mongodb connected');
});

app.use(express.static(path.join(__dirname, 'app')));
app.use(cookieParser());
app.get('/',function(req,res){
    res.sendFile(path.join(__dirname + '/app/view/index.html'));
});
app.use('/api',api);
app.listen(3000,function(){
    console.log('api server listening on port 3000!');
})
