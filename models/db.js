var mongoose = require('mongoose'); // Import Mongoose Package
var Schema = mongoose.Schema;  // Assign Mongoose Schema function to variable

mongoose.Promise = global.Promise;

var RandomNameSchema = new Schema({
    name:{type:String,required:true}
});

module.exports = mongoose.model('DB', RandomNameSchema);
