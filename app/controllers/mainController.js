angular.module('mainController', ['mainServices'])
.controller('mainCtrl',function(MainService){
    var app = this;
    var modifyMode = [];
    var modifiedName = [];
    app.getNames = function(){
        MainService.getNames().then(function(names){
            app.names = names.data;
        })
    }
    app.addName = function(name){
        var item = {
            name : name
        }
        MainService.addName(item).then(function(name){
            app.names.push(name.data);
        })
    }
    app.deleteName = function(id){
        MainService.deleteName(id).then(function(info){
            app.getNames();
        })
    }
    app.editName = function(name,id){
        var item = {
            name :name,
            id:id
        }
        MainService.editName(item).then(function(name){
            console.log(name.data);
        })
    }
    app.getNames();
})