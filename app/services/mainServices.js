angular.module('mainServices', [])
 .factory('MainService',function($http){
    var mainFactory = {};
    mainFactory.getNames = function(){
        return $http.get('/api/names');
    }
    mainFactory.addName = function(data){
        return $http.post('/api/names',data);
    }
    mainFactory.deleteName = function(id){
        return $http.delete('/api/names/'+id);
    }
    mainFactory.editName = function(item){
        var temp = {
            name : item.name
        }
        return $http.put('/api/names/'+item.id,temp);
    }
    return mainFactory;
 });